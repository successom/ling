class TranslationTag < ActiveRecord::Base
  belongs_to :tag
  belongs_to :translatable, polymorphic: true

  validates_uniqueness_of :tag_id, scope: [:translatable_id, :translatable_type]
end
