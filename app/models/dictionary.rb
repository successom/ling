class Dictionary < ActiveRecord::Base
  has_many :users_dictionaries

  validates_uniqueness_of :name, :scope => :dict_id
end
