class Tag < ActiveRecord::Base
  has_many :translation_tags

  validates_uniqueness_of :tag
end
