class User < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User
  before_create :skip_confirmation!

  devise :database_authenticatable, :recoverable,
         :trackable, :validatable, :registerable,
         :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]

  has_many :users_dictionaries
  has_many :identities

  attr_accessor :provider, :uid

  def provider
    @provider ||= Identity.find_by_user_id(self.id).provider
  end

  def uid
    @identity ||= Identity.find_by_user_id(self.id).uid
  end

  def self.find_by_uid(uid)
    if (identity = Identity.find_by_uid(uid)).present?
      user = identity.user
    end
    user
  end

  def self.from_omniauth(auth, current_user = nil, skip_confirmation = true)
    identity = Identity.find_for_oauth(auth['uid'], auth['provider'])
    user = current_user ? current_user : identity.user

    if user.nil?
      email = auth['info']['email']
      user = User.where(:email => email).first if email

      if user.nil?
        user = User.new(
          name:                  auth['info']['name'],
          nickname:              auth['info']['nickname'] || auth['uid'],
          email:                 email ? email : "#{auth['uid']}-#{auth['provider']}.com",
          password:              auth['password'] || Devise.friendly_token[0,20],
          password_confirmation: auth['password_confirmation']
        )
        user.skip_confirmation! if skip_confirmation
        unless user.save
          return user
        end
      end
    end

    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end
end
