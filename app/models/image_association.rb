class ImageAssociation < ActiveRecord::Base
  belongs_to :vocabulary_de_ru_translation, foreign_key: :translation_id
  belongs_to :vocabulary_en_ru_translation, foreign_key: :translation_id
  has_one :vocabulary_en_ru_user_translation

  validates_presence_of :image_url

  def self.fetch_or_build_association(translation_id, word)
    association = self.where(translation_id: translation_id).order("count_of_use DESC").first
    if association.nil?
      image = AssociationImagesFetcher.new(word, 1).execute
      association = self.create(translation_id: translation_id,
                                image_url:      image[0][:MediaUrl])
    end
    association
  end

  def increase_count
    self.count_of_use = self.count_of_use + 1
    save
  end
end
