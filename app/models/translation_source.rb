class TranslationSource < ActiveRecord::Base
  has_many :vocabulary_en_ru_translations
  has_many :vocabulary_de_ru_translations
end
