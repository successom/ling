class UsersDictionary < ActiveRecord::Base
  include AASM

  belongs_to :user
  belongs_to :dictionary

  aasm do
    state :active, initial: true
    state :removed

    event :remove do
      transitions from: [:active], to: [:removed]
    end
  end
end
