# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create(email: 'igomelch@gmail.com', name: 'Igor', password: '123456', password_confirmation: '123456', uid: 'igomelch@gmail.com', provider: 'email')

ru_en_user_dict = UsersDictionary.create(user: user, dictionary: Dictionary.find_by_name('Russian-English'))
en_ru_user_dist = UsersDictionary.create(user: user, dictionary: Dictionary.find_by_name('English-Russian'))

noun = Vocabulary::SpeachPart.find_by_name('Noun')
verb = Vocabulary::SpeachPart.find_by_name('Verb')

ru_word_1 = Vocabulary::RuWord.create(word: 'собака', vocabulary_speach_part_id: noun.id)
ru_word_2 = Vocabulary::RuWord.create(word: 'пес', vocabulary_speach_part_id: noun.id)
ru_word_3 = Vocabulary::RuWord.create(word: 'ходить', vocabulary_speach_part_id: verb.id)
ru_word_4 = Vocabulary::RuWord.create(word: 'гулять', vocabulary_speach_part_id: verb.id)
ru_word_5 = Vocabulary::RuWord.create(word: 'прогуливаться', vocabulary_speach_part_id: verb.id)

en_word_1 = Vocabulary::EnWord.create(word: 'dog', vocabulary_speach_part_id: noun.id)
en_word_2 = Vocabulary::EnWord.create(word: 'walk', vocabulary_speach_part_id: verb.id)

en_ru_translation = [{vocabulary_en_word_id: en_word_1.id, vocabulary_ru_word_id: ru_word_1.id, count_of_use: 1},
                     {vocabulary_en_word_id: en_word_1.id, vocabulary_ru_word_id: ru_word_2.id, count_of_use: 1},
                     {vocabulary_en_word_id: en_word_2.id, vocabulary_ru_word_id: ru_word_3.id, count_of_use: 2},
                     {vocabulary_en_word_id: en_word_2.id, vocabulary_ru_word_id: ru_word_4.id, count_of_use: 1},
                     {vocabulary_en_word_id: en_word_2.id, vocabulary_ru_word_id: ru_word_5.id, count_of_use: 0}
                    ]

en_ru_translation.each do |en_ru_translation|
  Vocabulary::EnRuTranslation.create!(en_ru_translation)
end

en_ru_user_translation = [{users_dictionary_id: 1, translation_id: 1},
                          {users_dictionary_id: 2, translation_id: 2},
                          {users_dictionary_id: 2, translation_id: 3},
                          {users_dictionary_id: 1, translation_id: 3},
                          {users_dictionary_id: 1, translation_id: 4},
                         ]

en_ru_user_translation.each do |en_ru_user_translation|
  Vocabulary::EnRuUserTranslation.create!(en_ru_user_translation)
end
