class AddUsersDictionaryIdToTranslationTags < ActiveRecord::Migration
  def up
    add_column :translation_tags, :users_dictionary_id, :integer, null: false, default: 0
    add_index :translation_tags, :users_dictionary_id
    execute "UPDATE translation_tags SET users_dictionary_id = tags.users_dictionary_id FROM tags WHERE tags.id = translation_tags.tag_id;"
    remove_column :tags, :users_dictionary_id
  end

  def down
    add_column :tags, :users_dictionary_id, :integer, null: false, default: 0
    add_index :tags, :users_dictionary_id
    execute "UPDATE tags SET users_dictionary_id = translation_tags.users_dictionary_id FROM translation_tags WHERE tags.id = translation_tags.tag_id;"
    remove_column :translation_tags, :users_dictionary_id
  end
end
