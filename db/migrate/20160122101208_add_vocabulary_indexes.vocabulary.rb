# This migration comes from vocabulary (originally 20160122100013)
class AddVocabularyIndexes < ActiveRecord::Migration
  def change
    add_index :vocabulary_speach_parts, :id

    add_index :vocabulary_ru_words, :id
    add_index :vocabulary_ru_words, :word
    add_index :vocabulary_ru_words, :vocabulary_speach_part_id

    add_index :vocabulary_en_words, :id
    add_index :vocabulary_en_words, :word
    add_index :vocabulary_en_words, :vocabulary_speach_part_id

    add_index :vocabulary_de_words, :id
    add_index :vocabulary_de_words, :word
    add_index :vocabulary_de_words, :vocabulary_speach_part_id

    add_index :vocabulary_en_ru_translations, :id
    add_index :vocabulary_en_ru_translations, :vocabulary_en_word_id
    add_index :vocabulary_en_ru_translations, :vocabulary_ru_word_id
    add_index :vocabulary_en_ru_translations, :translation_source_id
    add_index :vocabulary_en_ru_translations, :count_of_use

    add_index :vocabulary_de_ru_translations, :id
    add_index :vocabulary_de_ru_translations, :vocabulary_de_word_id
    add_index :vocabulary_de_ru_translations, :vocabulary_ru_word_id
    add_index :vocabulary_de_ru_translations, :translation_source_id
    add_index :vocabulary_de_ru_translations, :count_of_use

    add_index :vocabulary_en_ru_user_translations, :id
    add_index :vocabulary_en_ru_user_translations, :users_dictionary_id
    add_index :vocabulary_en_ru_user_translations, :translation_id
    add_index :vocabulary_en_ru_user_translations, :image_association_id, name: 'index_vocab_en_ru_user_transl_on_image_association_id'

    add_index :vocabulary_de_ru_user_translations, :id
    add_index :vocabulary_de_ru_user_translations, :users_dictionary_id
    add_index :vocabulary_de_ru_user_translations, :translation_id
    add_index :vocabulary_de_ru_user_translations, :image_association_id, name: 'index_vocab_de_ru_user_transl_on_image_association_id'
  end
end
