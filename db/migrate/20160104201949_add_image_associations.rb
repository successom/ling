class AddImageAssociations < ActiveRecord::Migration
  def change
    create_table :image_associations do |t|
      t.string :image_url
      t.integer :count_of_use, null: false, default: 0
      t.integer :translation_id
      t.timestamps
    end
  end
end
