class AddUsersDictionaries < ActiveRecord::Migration
  def change
    create_table :users_dictionaries do |t|
      t.references :user
      t.references :dictionary
      t.string     :aasm_state
      t.timestamps
    end
  end
end
