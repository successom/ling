# This migration comes from vocabulary (originally 20151118160228)
class CreateVocabularyDeRuDictionary < ActiveRecord::Migration
  def up
    create_table :vocabulary_de_words do |t|
      t.string :word
      t.string :transcription
      t.references :vocabulary_speach_part
      t.timestamps
    end

    create_table :vocabulary_de_ru_translations do |t|
      t.references :vocabulary_de_word
      t.references :vocabulary_ru_word
      t.references :translation_source
      t.integer :count_of_use, null: false, default: 0
      t.timestamps
    end

    create_table :vocabulary_de_ru_user_translations do |t|
      t.references :users_dictionary
      t.integer :translation_id
      t.references :image_association
      t.timestamps
    end

    Dictionary.create(from_languige: 'ru', to_languige: 'de', name: 'Russian-German', dict_id: 'de_ru')
    Dictionary.create(from_languige: 'de', to_languige: 'ru', name: 'German-Russian', dict_id: 'de_ru')
  end

  def down
    drop_table :vocabulary_de_words
    drop_table :vocabulary_de_ru_translations
    drop_table :vocabulary_de_ru_user_translations

    Dictionary.where(dict_id: 'de_ru').destroy_all
  end
end

