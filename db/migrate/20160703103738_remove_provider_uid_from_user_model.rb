class RemoveProviderUidFromUserModel < ActiveRecord::Migration
  def up
    remove_column :users, :provider
    remove_column :users, :uid
  end

  def down
    add_column :users, :provider, :string, :null => false
    add_column :users, :uid, :string, :null => false, :default => ""
  end
end
