# This migration comes from vocabulary (originally 20151118152528)
class CreateVocabularyRuWords < ActiveRecord::Migration
  def change
    create_table :vocabulary_ru_words do |t|
      t.string :word
      t.string :transcription
      t.references :vocabulary_speach_part
      t.timestamps
    end
  end
end
