class CreateIndexes < ActiveRecord::Migration
  def change
    add_index :users, :id
    add_index :dictionaries, :id
    add_index :users_dictionaries, :id
    add_index :users_dictionaries, :user_id
    add_index :users_dictionaries, :dictionary_id
    add_index :translation_sources, :id
    add_index :image_associations, :id
    add_index :image_associations, :translation_id
  end
end
