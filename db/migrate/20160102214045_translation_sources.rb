class TranslationSources < ActiveRecord::Migration
  def change
    create_table :translation_sources do |t|
      t.string :source
      t.timestamps
    end
  end
end
