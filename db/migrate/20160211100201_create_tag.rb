class CreateTag < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :tag
      t.integer :creator_id
      t.timestamps
    end

    create_table :translation_tags do |t|
      t.references :tag
      t.references :translatable, polymorphic: true
      t.timestamps
    end

    add_index :tags, :id
    add_index :tags, :creator_id
    add_index :translation_tags, :tag_id
    add_index :translation_tags, :translatable_id
    add_index :translation_tags, :translatable_type
  end
end
