# This migration comes from vocabulary (originally 20151118151734)
class CreateVocabularySpeachParts < ActiveRecord::Migration
  def change
    create_table :vocabulary_speach_parts do |t|
      t.string :name
      t.timestamps
    end

    Vocabulary::SpeachPart.create(name: 'Noun')
    Vocabulary::SpeachPart.create(name: 'Pronoun')
    Vocabulary::SpeachPart.create(name: 'Verb')
    Vocabulary::SpeachPart.create(name: 'Adjective')
    Vocabulary::SpeachPart.create(name: 'Adverv')
    Vocabulary::SpeachPart.create(name: 'Preposition')
    Vocabulary::SpeachPart.create(name: 'Conjunction')
    Vocabulary::SpeachPart.create(name: 'Interjection')
  end
end
