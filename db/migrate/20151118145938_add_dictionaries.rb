class AddDictionaries < ActiveRecord::Migration
  def change
    create_table :dictionaries do |t|
      t.string :from_languige
      t.string :to_languige
      t.string :name
      t.string :dict_id
      t.timestamps
    end
  end
end
