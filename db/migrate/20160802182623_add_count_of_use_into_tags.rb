class AddCountOfUseIntoTags < ActiveRecord::Migration
  def change
    add_column :tags, :count_of_use, :integer, null: false, default: 1
  end
end
