class RecreateTranslationTags < ActiveRecord::Migration
  def up
    drop_table :translation_tags

    create_table :translation_tags do |t|
      t.references :tag
      t.references :translatable, polymorphic: true
      t.timestamps
    end

    add_index :translation_tags, :tag_id
    add_index :translation_tags, :translatable_id
    add_index :translation_tags, :translatable_type

    add_column :tags, :users_dictionary_id, :integer, null: false, default: 0
    remove_column :tags, :creator_id
  end

  def down
    drop_table :translation_tags

    create_table :translation_tags do |t|
      t.references :tag
      t.references :translatable, polymorphic: true
      t.timestamps
    end

    add_index :translation_tags, :tag_id
    add_index :translation_tags, :translatable_id
    add_index :translation_tags, :translatable_type

    add_column :tags, :creator_id, :integer, null: false, default: 0
    remove_column :tags, :users_dictionary_id
  end
end
