== README

Examples of implemented REST requests:

- Auth
  - Create user

    curl -i -X POST -d "email=igomelch@gmail.com&password=123456&password_confirmation=123456&confirm_success_url=http://localhost:9000/#/&config_name=default" "http://localhost:3000/auth"

  - Take token

    curl -i -X POST -d "email=igomelch@mail.ru&password=123456" "http://localhost:3000/auth/sign_in"

- Vocabulary

  - Dictionaries

    - GET user_dictionaries

      request

      curl -i -X GET \
      -H "Access-Token: pawJZS66GJ0gdOANoiPVOg" \
      -H "Token-Type: Bearer" \
      -H "Client: 1tRcvpwFiRibGbvWlYYmQw" \
      -H "Expiry: 1470573175" \
      -H "Uid: 101767442853458819334" \
      -d "user_id=43" \
      "http://localhost:3000/vocabulary/api/v1/user_dictionaries"

      response

      [{"id":5,"dictionary_id":2,"dictionary_name":"English-Russian","user_id":43,"dict_id":"en_ru","from":"en","to":"ru"}]

    - GET dictionaries

      request

      curl -i -X GET \
      -H "Access-Token: pawJZS66GJ0gdOANoiPVOg" \
      -H "Token-Type: Bearer" \
      -H "Client: 1tRcvpwFiRibGbvWlYYmQw" \
      -H "Expiry: 1470573175" \
      -H "Uid: 101767442853458819334" \
      -d "user_id=43" \
      "http://localhost:3000/vocabulary/api/v1/dictionaries"

      response

      [{"id":1,"from":"ru","to":"en","name":"Russian-English","dict_id":"en_ru"},{"id":2,"from":"en","to":"ru","name":"English-Russian","dict_id":"en_ru"},{"id":3,"from":"ru","to":"de","name":"Russian-German","dict_id":"de_ru"},{"id":4,"from":"de","to":"ru","name":"German-Russian","dict_id":"de_ru"}]

    - POST user_dictionaries (add user dictionary)

      request

      curl -i -X POST \
      -H "Access-Token: pawJZS66GJ0gdOANoiPVOg" \
      -H "Token-Type: Bearer" \
      -H "Client: 1tRcvpwFiRibGbvWlYYmQw" \
      -H "Expiry: 1470573175" \
      -H "Uid: 101767442853458819334" \
      -d "user_id=43&dictionary_id=1" \
      "http://localhost:3000/vocabulary/api/v1/user_dictionaries"

      response

      {"id":7,"dictionary_name":"Russian-English","user_id":43}

    - DELETE user_dictionaries (remove user dictionary)

      request

      curl -i -X DELETE \
      -H "Access-Token: pawJZS66GJ0gdOANoiPVOg" \
      -H "Token-Type: Bearer" \
      -H "Client: 1tRcvpwFiRibGbvWlYYmQw" \
      -H "Expiry: 1470573175" \
      -H "Uid: 101767442853458819334" \
      -d "id=7" \
      "http://localhost:3000/vocabulary/api/v1/user_dictionaries"

      response

      {"id":9,"message":"User Dictionary ID = 9 has been destroyed"}

  - Translations

    - GET user_dictionary_translations

      request

      curl -i -X GET \
      -H "Access-Token: pawJZS66GJ0gdOANoiPVOg" \
      -H "Token-Type: Bearer" \
      -H "Client: 1tRcvpwFiRibGbvWlYYmQw" \
      -H "Expiry: 1470573175" \
      -H "Uid: 101767442853458819334" \
      -d "dict_id=en_ru&from=en&to=ru&user_dictionary_id=10" \
      "http://localhost:3000/vocabulary/api/v1/user_dictionary_translations"

      response

      [{"id":20,"users_dictionary_id":10,"translation_id":270,"word_from":"cat","word_to":"кот","transcription_from":null,"association_img_url":"http://3.bp.blogspot.com/-5tna2I9cHPo/TV5XB85nTTI/AAAAAAAAAvY/uPnXQA8sxn8/s1600/cat-allergy.jpg","dict_id":"en_ru","tags":null},{"id":19,"users_dictionary_id":10,"translation_id":269,"word_from":"cat","word_to":"кошара","transcription_from":null,"association_img_url":"http://3.bp.blogspot.com/-5tna2I9cHPo/TV5XB85nTTI/AAAAAAAAAvY/uPnXQA8sxn8/s1600/cat-allergy.jpg","dict_id":"en_ru","tags":null}]

    - POST user_dictionary_translations (add new translation to the user_dictionary)

      request

      curl -i -X POST \
      -H "Access-Token: pawJZS66GJ0gdOANoiPVOg" \
      -H "Token-Type: Bearer" \
      -H "Client: 1tRcvpwFiRibGbvWlYYmQw" \
      -H "Expiry: 1470573175" \
      -H "Uid: 101767442853458819334" \
      -d "dict_id=en_ru&external_translation=true&from=en&to=ru&translation_id=0&users_dictionary_id=10&word_from=cat&word_to=кот" \
      "http://localhost:3000/vocabulary/api/v1/user_dictionary_translations"

      response

      {"id":null,"users_dictionary_id":10,"translation_id":270,"image_association":"http://3.bp.blogspot.com/-5tna2I9cHPo/TV5XB85nTTI/AAAAAAAAAvY/uPnXQA8sxn8/s1600/cat-allergy.jpg"}

    - GET translations

      request

      curl -i -X GET \
      -H "Access-Token: pawJZS66GJ0gdOANoiPVOg" \
      -H "Token-Type: Bearer" \
      -H "Client: 1tRcvpwFiRibGbvWlYYmQw" \
      -H "Expiry: 1470573175" \
      -H "Uid: 101767442853458819334" \
      -d "dict_id=en_ru&from=en&to=ru&word=cat" \
      "http://localhost:3000/vocabulary/api/v1/translations"

      response

      [{"translation_id":267,"word_id_from":264,"word_from":"cat","word_id_to":256,"word_to":"rjn","count":1},{"translation_id":268,"word_id_from":265,"word_from":"location","word_id_to":257,"word_to":"местонахождение","count":1},{"translation_id":61,"word_id_from":58,"word_from":"abdicate","word_id_to":60,"word_to":"отрекаться от престола)отказываться от права","count":0},{"translation_id":62,"word_id_from":59,"word_from":"abdication","word_id_to":61,"word_to":"отречение от престола)отказ от права","count":0}]
