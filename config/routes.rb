Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  mount Vocabulary::Engine, at: "/vocabulary"
end
