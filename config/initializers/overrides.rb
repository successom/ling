DeviseTokenAuth::OmniauthCallbacksController.class_eval do
  def get_resource_from_auth_hash
    @resource = resource_class.from_omniauth(auth_hash)
  end
end

DeviseTokenAuth::SessionsController.class_eval do
  def create
    # Check
    field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first

    @resource = nil
    if field
      q_value = resource_params[field]

      if resource_class.case_insensitive_keys.include?(field)
        q_value.downcase!
      end

      q = "#{field.to_s} = ?"

      if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
        q = "BINARY " + q
      end

      @resource = resource_class.where(q, q_value).first
    end

    if @resource and valid_params?(field, q_value) and @resource.valid_password?(resource_params[:password]) and (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
      # create client id
      @client_id = SecureRandom.urlsafe_base64(nil, false)
      @token     = SecureRandom.urlsafe_base64(nil, false)

      @resource.tokens[@client_id] = {
        token: BCrypt::Password.create(@token),
        expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
      }
      @resource.save

      sign_in(:user, @resource, store: false, bypass: false)

      yield @resource if block_given?

      render_create_success
    elsif @resource and not (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
      render_create_error_not_confirmed
    else
      render_create_error_bad_credentials
    end
  end
end

DeviseTokenAuth::RegistrationsController.class_eval do
  def create
    auth = {'info'                  => {'email' => sign_up_params[:email]},
            'uid'                   => sign_up_params[:email],
            'provider'              => 'email',
            'password'              => sign_up_params[:password],
            'password_confirmation' => sign_up_params[:password_confirmation]
           }
    begin
      if resource_class.where(email: sign_up_params[:email]).count == 0
        if (@resource = resource_class.from_omniauth(auth, nil, false)).valid?
          render_create_success
        else
          render_create_error
        end
      else
        @resource = resource_class.new(sign_up_params)
        clean_up_passwords @resource
        @resource.errors.add(:email, "already in use")
        render_create_error
      end
    rescue ActiveRecord::RecordNotUnique
      clean_up_passwords @resource
      render_create_error_email_already_exists
    end
  end
end

DeviseTokenAuth::Concerns::User.module_eval do
  protected
  def unique_email_user
    if self.class.where(email: email).count > 0
      errors.add(:email, "already in use")
    end
  end
end
