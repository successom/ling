# Use this hook to configure devise mailer, warden hooks and so forth.
# Many of these configuration options can be set straight in your model.
Devise.setup do |config|
  config.password_length = 6..28
  config.secret_key = APP_CONFIG['device_secret_key']

  config.omniauth :facebook, APP_CONFIG['facebook_omniauth_key'], APP_CONFIG['facebook_omniauth_secret_key']
  config.omniauth :google_oauth2, APP_CONFIG['google_omniauth_key'], APP_CONFIG['google_omniauth_secret_key']
end
