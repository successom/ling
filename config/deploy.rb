# config valid only for current version of Capistrano
lock '3.4.0'

#set :default_shell, '/bin/bash -l'
set :application, 'ling'
set :repo_url, 'git@bitbucket.org:successom/ling.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/ubuntu/ling'
set :rvm_type, :user
set :rvm_ruby_version, "2.2.2"

#set :rvm_binary, '~/.rvm/bin/rvm'
#set :use_sudo, false
# set :default_env, {
#     :PATH => '/home/ubuntu/.rvm/gems/ruby-2.2.1/bin:/home/ubuntu/.rvm/gems/ruby-2.2.1@global/bin:/home/ubuntu/.rvm/rubies/ruby-2.2.1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/home/ubuntu/.rvm/bin:/home/ubuntu/.rvm/bin',
#     :GEM_HOME => '/home/ubuntu/.rvm/gems/ruby-2.2.1',
#     :GEM_PATH => '/home/ubuntu/.rvm/gems/ruby-2.2.1:/home/ubuntu/.rvm/gems/ruby-2.2.1@global'
# }

#set :pty, true

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'config/application.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

# before 'deploy', 'rvm1:install:rvm'
# before 'deploy', 'rvm1:install:ruby'

# after 'deploy' do
#   run "cd #{release_path}; RAILS_ENV=#{rails_env} #{bundle_cmd}"
#   #run "cd #{release_path}; RAILS_ENV=#{rails_env} #{bundle_cmd} exec rake db:migrate"
# end

namespace :deploy do
  desc 'Restart application'
  task :restart do
    invoke 'deploy:unicorn:restart'
  end
  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'

  namespace :unicorn do
    pid_path = "#{deploy_to}/shared/pids"
    unicorn_pid = "#{pid_path}/unicorn.pid"

    def run_unicorn
      execute "cd #{current_path} && RAILS_ENV=#{fetch(:rails_env)} ~/.rvm/bin/rvm 2.2.2 do bundle exec unicorn -c /home/ubuntu/ling/current/config/unicorn.rb -p 3000 -D;"
    end

    desc 'Start unicorn'
    task :start do
      on roles(:app) do
        run_unicorn
      end
    end

    desc 'Stop unicorn'
    task :stop do
      on roles(:app) do
        if test "[ -f #{unicorn_pid} ]"
          execute :kill, "-QUIT `cat #{unicorn_pid}`"
        end
      end
    end

    desc 'Force stop unicorn (kill -9)'
    task :force_stop do
      on roles(:app) do
        if test "[ -f #{unicorn_pid} ]"
          execute :kill, "-9 `cat #{unicorn_pid}`"
          execute :rm, unicorn_pid
        end
      end
    end

    desc 'Restart unicorn'
    task :restart do
      on roles(:app) do
        if test "[ -f #{unicorn_pid} ]"
          execute :kill, "-USR2 `cat #{unicorn_pid}`"
        else
          run_unicorn
        end
      end
    end
  end
end
