desc "vacuum analyze db tables"
task :vacuum_analyze_tables => :environment do
  tables = ['dictionaries',
            'image_associations',
            'schema_migrations',
            'translation_sources',
            'users',
            'users_dictionaries',
            'vocabulary_de_ru_translations',
            'vocabulary_de_ru_user_translations',
            'vocabulary_de_words',
            'vocabulary_en_ru_translations',
            'vocabulary_en_ru_user_translations',
            'vocabulary_en_words',
            'vocabulary_ru_words',
            'vocabulary_speach_parts']

  tables.each do |table|
    ActiveRecord::Base.connection.execute("VACUUM ANALYZE #{table};");
  end
end
